import React,{Component} from 'react';
import {View, Text} from 'react-native';
import ImageCard from "./ImageCard.component";

export default class TaskCard extends Component{
    render() {
        return(
            <View style={{
                backgroundColor: this.props.isActive ? '#6C63FF' : '#fff',
                elevation: this.props.elevation || 5,
                height: (this.props.height * (this.props.scale || 1)) || 140 * (this.props.scale || 1),
                width: (this.props.width * (this.props.scale || 1)) || 110 * (this.props.scale || 1),
                borderTopLeftRadius: 35,
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
                borderBottomLeftRadius: 10,
                marginLeft: 15,
                padding: 30
            }}>
                <Text numberOfLines={2} style={{
                    lineHeight: 18,
                    fontSize: 14,
                    color: this.props.isActive && '#fff'
                }}>{this.props.title}</Text>

                <View style={{position:'absolute', bottom: 15, left: 27.5,}}>
                    <ImageCard elevation={0} height={25} width={25} source={require("../../src/resources/images/profile.png")}/>
                </View>
                <View style={{position:'absolute', bottom: 15, left: 15}}>
                    <ImageCard borderWidth={2} borderColor={this.props.isActive && '#6C63FF'} elevation={0} height={25} width={25} source={require("../../src/resources/images/profile.png")}/>
                </View>

                <View style={{position:'absolute', bottom: 15, right: 15}}>
                    <Text style={{color: this.props.isActive && '#fff'}}>{this.props.number}</Text>
                </View>
            </View>
        );
    }
}
