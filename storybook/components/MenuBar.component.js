import React,{Component} from 'react';
import {View} from 'react-native';

export default class MenuBar extends Component{
    render() {
        return(
            <View style={{
                height: (this.props.height || 45) * (this.props.scale || 1),
                width: (this.props.height || 45) * (this.props.scale || 1),
                borderRadius: (this.props.height / 2 || 45 / 2) * (this.props.scale || 1),
                justifyContent:'center',
                alignItems:'flex-start',
            }}>
                <View style={{width: 16, height: 2, backgroundColor:'#000', marginBottom: 5}} />
                <View style={{width: 22, height: 2, backgroundColor:'#000'}} />
            </View>
        );
    }
}
