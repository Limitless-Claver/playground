import React, {Component} from 'react';
import {View, TextInput, StyleSheet, Dimensions, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Validate from '../../src/services/validator.service';

const width = Dimensions.get('window').width;
export default class InputText extends Component {
  state = {
    errorMessage: ''
  }
  Validate(){
    let response = Validate(this.props.validationType, this.props.value);
    if(!response.validated){
      this.setState({errorMessage: response.message,})
    }
  }
  render() {
    return (
        <View style={{width: width * 0.8, margin: 5,}}>
          <View style={styles.container}>
            <Icon name={this.props.icon} size={18} color="#8B12F5" />
            <TextInput
                placeholder={this.props.holder}
                style={styles.input}
                keyboardType={this.props.keyboardType}
                secureTextEntry={
                  this.props.secureTextEntry ? this.props.secureTextEntry : false
                }
                onBlur={()=> this.Validate()}
                onChangeText={this.props.onChangeText}
                onFocus={()=> this.setState({errorMessage: ''})}
            />
          </View>
          <Text style={{marginTop: 5,marginLeft: 15, color: 'red'}}>{this.state.errorMessage}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    elevation: 3,
    borderRadius: 25,
    paddingHorizontal: 25,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    fontSize: 18,
    marginHorizontal: 10,
    flex: 1,
  },
});
