import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableNativeFeedback, Dimensions} from 'react-native';

const width = Dimensions.get('window').width;
export default class Button extends Component {
  render() {
    return (
      <TouchableNativeFeedback onPress={this.props.onPress}>
        <View style={styles.container}>
          <Text style={styles.text}>{this.props.label}</Text>
        </View>
      </TouchableNativeFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#8B12F5',
    width: width * 0.8,
    height: 55,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 30,
  },
  text: {
    color: '#fff',
    fontSize: 18,
  },
});
