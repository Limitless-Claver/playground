import React,{Component} from "react";
import {View, Image} from "react-native";
import PropTypes from "prop-types";

export default class ImageCard extends Component{
    static propTypes = {
        height: PropTypes.number,
        width: PropTypes.number,
        elevation: PropTypes.number,
        borderRadius: PropTypes.number,
        backgroundColor: PropTypes.string,
        source: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            // Opaque type returned by require('./some_image.jpg')
            PropTypes.number,
        ]),
        borderColor: PropTypes.string,
        borderWidth: PropTypes.number
    }

    render() {
        return(
          <View style={{
              backgroundColor: this.props.backgroundColor || "#fff",
              height: this.props.height || 60,
              width: this.props.width || 60,
              elevation: this.props.elevation || 5,
              borderRadius: this.props.borderRadius || 8,
              overflow: "hidden",
              alignItems: "center",
              justifyContent: "center",
              borderWidth: this.props.borderWidth || 0,
              borderColor: this.props.borderColor || '#fff'
          }}>
              <Image source={this.props.source} style={{
                  height: this.props.height * 0.8 || 60,
                  width: this.props.width * 0.8 || 60 * 0.8
              }}/>
              <Image />
          </View>
        );
    }
}
