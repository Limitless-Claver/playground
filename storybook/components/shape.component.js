import React,{Component} from 'react';
import {View} from 'react-native';

export default class Shape extends Component{
    render() {
        return(
            <View style={{
                position:'absolute',
                left: this.props.x || 0,
                top: this.props.y || 0,
                backgroundColor: this.props.backgroundColor || '#f2f2f2',
                height: this.props.height,
                width: this.props.width,
                borderRadius: this.props.borderRadius,
                borderBottomRightRadius: this.props.borderBottomRightRadius,
                borderBottomLeftRadius: this.props.borderBottomRightRadius,
                borderTopRightRadius: this.props.borderTopRightRadius,
                borderTopLeftRadius: this.props.borderBottomLeftRadius,
                transform: [{ rotate: this.props.rotate || '0deg'}]
            }} />
        );
    }
}
