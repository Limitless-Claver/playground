import React,{Component} from 'react';
import {View,Dimensions} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";


export default class IconButton extends Component{
    render() {
        return (
            <View style={{
                height: (this.props.height || 45) * (this.props.scale || 1),
                width: (this.props.height || 45) * (this.props.scale || 1),
                borderRadius: (this.props.height / 2 || 45 / 2) * (this.props.scale || 1),
                backgroundColor: this.props.backgroundColor || '#fff',
                justifyContent:'center',
                alignItems:'center',
                position: this.props.isActive ? 'absolute' : 'relative',
                top: this.props.isActive && -(this.props.height || 45) * (this.props.scale || 1),
                left: this.props.isActive && ((Dimensions.get('window').width * 0.7) / 2) - (this.props.height / 2 || 45 / 2) * (this.props.scale || 1),
            }}>
                <Icon name={this.props.name} color={this.props.color || '#ccc'} size={this.props.scale * 22 || 22}/>
            </View>
        );
    }
}
