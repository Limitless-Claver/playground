import React,{Component} from 'react';
import {View, Text} from 'react-native';
import initializations from '../../src/boot/initializations';
import User from '../../src/model/User.model';


export default class LoadingScreen extends Component{
    constructor(props) {
        super(props);
        //initializing of necessary functions on this screen before app launches
        initializations.call();

        //Redirects to either an authenticated screen or unauthenticated screen
        if(User.isLoggedIn) this.props.navigation.replace("Home");
        else this.props.navigation.replace("Login");
    }

    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Text>Owatech</Text>
            </View>
        );
    }
}
