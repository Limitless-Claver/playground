import React,{Component} from 'react';
import {View,Text,Image, ScrollView, Dimensions} from 'react-native';
import DatabaseService from '../../src/services/database.service';
import User from '../../src/model/User.model';
import ImageCard from "../components/ImageCard.component";
import TaskCard from "../components/TaskCard.component";
import IconButton from "../components/IconButton.component";
import Shape from "../components/shape.component";
import MenuBar from "../components/MenuBar.component";

const width = Dimensions.get('window').width;

export default class Home extends Component{
    constructor(props) {
        super(props);
    }

    logout() {
        DatabaseService.logout();
        this.props.navigation.replace("Login");
    }

    highlight(content){
        return(
            <Text style={{color:'#6C63FF'}}>{content}</Text>
        );
    }

    render() {
        return (
            <View style={{backgroundColor:'#fff', flex: 1}}>
                    <View style={{backgroundColor: '#fff', justifyContent: 'flex-end', alignItems:'center', flex: 0.6}}>
                        <Shape height={180}  width={220} borderRadius={60} x={-80} y={-50} rotate='150deg'/>
                        <Shape height={100} width={100} borderRadius={60} x={width - 30} y={100}/>
                        <View style={{position:'absolute',flexDirection:'row', justifyContent:'space-between', width:'90%', top: 20}}>
                            <MenuBar/>
                            <IconButton name="md-notifications" color="#000"/>
                        </View>

                        <Image source={require('../../src/resources/images/workers.png')} style={{height: 160, width:'80%'}}/>
                    </View>
                    <View style={{backgroundColor:'#f2f2f2', borderBottomLeftRadius: 25, borderBottomRightRadius: 25}}>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 20, marginTop:40 }}>
                            <View>
                                <Text style={{fontSize:26}}>Hi {this.highlight(User.firstName)}</Text>
                                <Text style={{fontSize:16}}>We have four meetings today</Text>
                            </View>
                            <ImageCard source={require("../../src/resources/images/profile.png")} borderRadius={15} height={45} width={45}/>
                        </View>

                        <ScrollView
                           style={{bottom: -30}}
                            showsHorizontalScrollIndicator={false}
                            scrollEventThrottle={200}
                            decelerationRate="fast"
                            snapToStart
                            horizontal={true}
                            contentContainerStyle={{alignItems:'center', justifyContent:'center'}}>
                            <TaskCard isActive={true} scale={1.1} title="Design Review" number="+1"/>
                            <TaskCard title="New UI style" number="+7"/>
                            <TaskCard title="Month of Planning" number="+2"/>
                        </ScrollView>
                    </View>


                <View style={{alignSelf:'center',width: '70%', position: 'absolute', bottom: 20, marginHorizontal: 30, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                    <IconButton name="md-home" color="orange"/>
                    <IconButton name="md-add" backgroundColor='orange' color="#fff" isActive scale={1.3}/>
                    <IconButton name="md-calendar" />
                </View>

            </View>
        );
    }
}
