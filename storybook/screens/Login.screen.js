import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity, ToastAndroid,
} from 'react-native';
import InputText from '../components/InputText';
import Button from '../components/Button';
import Validate from '../../src/services/validator.service';
import DatabaseService from '../../src/services/database.service';
import User from '../../src/model/User.model';

export default class LoginScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      email:'',
      password: ''
    }
  }

  onChangeText(key,value){
    this.setState({[key]: value.trim(),})
  }

  fieldHasError(){
    let emailIsValid = Validate("email", this.state.email);
    let passwordIsValid = Validate("password", this.state.password);

    return !(emailIsValid.validated && passwordIsValid.validated);
  }


  async loginUser(){
    if(this.fieldHasError()) return this.showToast("Please correct all errors before you proceed");

    const authenticated = DatabaseService.login(this.state.email, this.state.password);

    if(authenticated.isValidated) {
      User.addCurrentUser(authenticated.user);
      this.props.navigation.replace("Home");
    }else{
      this.showToast("Sorry your details are incorrect");
    }
  }

  showToast(message){
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        180
    );
  }


  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.title}>Login</Text>
          <InputText
            holder="Email"
            icon="md-mail"
            keyboardType="email-address"
            onChangeText={(value) => this.onChangeText('email', value)}
            value={this.state.email}
            validationType="email"
          />
          <InputText
              holder="Password" icon="md-lock"
              onChangeText={(value) => this.onChangeText('password', value)}
              secureTextEntry={true}
              value={this.state.password}
              validationType="password"
          />

          <Button label="Login" onPress={()=> this.loginUser()}/>

          <TouchableOpacity
            style={{padding: 10}}
            onPress={() => this.props.navigation.navigate('Register')}>
            <Text>Don't have an account?</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 30,
  },
  title: {
    fontSize: 30,
    marginVertical: 15,
  },
});
