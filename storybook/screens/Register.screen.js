import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import InputText from '../components/InputText';
import Button from '../components/Button';
import DatabaseService from '../../src/services/database.service';
import Validate from '../../src/services/validator.service';
import User from '../../src/model/User.model';


export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      surname: '',
      otherName: '',
      email: '',
      password: ''
    }
  }

  onChangeText(key,value){
    this.setState({[key]: value.trim(),})
  }

  fieldHasError(){
    const {firstName,surname, otherName,email, password} = this.state;
    let firstNameIsValid = Validate("min_string", firstName);
    let surnameIsValid = Validate("min_string", surname);
    let otherNameIsValid = Validate("min_string", otherName);
    let emailIsValid = Validate("email", email);
    let passwordIsValid = Validate("password", password);

    return !(firstNameIsValid.validated && surnameIsValid.validated && otherNameIsValid.validated && emailIsValid.validated && passwordIsValid.validated);
  }

  async addUser(){
    if(this.fieldHasError()) return this.showToast( "Please correct all errors before you proceed");

      let response = DatabaseService.addUser(this.state);
      if(response.isSaved){
        User.addCurrentUser(this.state);
        this.props.navigation.replace("Home");
      }else{
        this.showToast(response.message)
      }

  }

  showToast(message){
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        180
    );
  }

  render() {
    return (
      <ScrollView>
          <View style={styles.container}>
            <Text style={styles.title}>Register</Text>
            <InputText
                holder="First Name"
                icon="md-person"
                onChangeText={(value)=> this.onChangeText('firstName', value)}
                value={this.state.firstName}
                validationType="min_string"
            />
            <InputText
                holder="Surname" icon="md-person"
                onChangeText={(value)=> this.onChangeText('surname', value)}
                value={this.state.surname}
                validationType="min_string"
            />
            <InputText
                holder="Other Name" icon="md-person"
                onChangeText={(value)=> this.onChangeText('otherName', value)}
                value={this.state.otherName}
                validationType="min_string"
            />
            <InputText
                holder="Email" icon="md-mail"
                keyboardType="email-address" onChangeText={(value)=> this.onChangeText('email', value)}
                value={this.state.email}
                validationType="email"
            />
            <InputText
                holder="Password" icon="md-lock"
                secureTextEntry={true} onChangeText={(value)=> this.onChangeText('password', value)}
                value={this.state.password}
                validationType="password"
            />

            <Button label="Register" onPress={()=> this.addUser()}/>

            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => this.props.navigation.navigate('Login')}>
              <Text>Already have an account?</Text>
            </TouchableOpacity>
          </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 30,
  },
  title: {
    fontSize: 30,
    marginVertical: 15,
  },
});
