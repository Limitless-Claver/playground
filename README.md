## Owatech Authentication App

Build a mobile application using React Native and Mobx.

---
### Requirements
* App Registration, Login and Homepage

**Registration Details**

* Email
* First Name
* Surname
* Other Name
* Password

***An authenticated user should be directed to the Homepage***

---

### Homepage
The user should be welcomed with, Hello ```${user.firstName}```, this is my first RN App.

User should be able to **Log out** log back in with the same details.

### Extra Notes
1. Reusable codes

2. Documentation of code

3. Systematic Arrangements

4. Security

5. Smooth running on at least android platform

6. Light Weight Functions / Classes

7. Realm DB
