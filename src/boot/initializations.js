import DatabaseService from '../services/database.service';
import User from '../model/User.model';

let initialize;
export default initialize = () => {
    //Initializing databases
    DatabaseService.createDatabase();

    // initialize user details if user is logged in
    const {loggedIn,user} = DatabaseService.isUserLoggedIn();
    loggedIn && User.addCurrentUser(user);
}
