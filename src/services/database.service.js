import Realm from 'realm';
import { Base64 } from 'js-base64';

export default class DatabaseService {
  static #userSchema = {
    name: 'Users',
    properties: {
      firstName: 'string',
      surname: 'string',
      otherName: 'string',
      email: {type: 'string', indexed: true},
      password: 'string',
      isLoggedIn: {type: 'bool', indexed: true}
    },
  };

  static createDatabase() {
    if(!Realm.exists({path: 'users.realm'})){
      new Realm({
        path: 'users.realm',
        primaryKey: 'email',
        schema: [this.#userSchema],
      });
    }
  }

  static addUser({firstName,surname,otherName,email,password}){
    try {
      let realm = new Realm({schema: [this.#userSchema]});
      let users = realm.objects("Users");
      let emailFound = users.filtered('email = $0',email);

      if(!emailFound.isEmpty()) return {isSaved: false, message: `${email} is already being used.`};

      realm.write(()=>{
        const newUser = realm.create('Users',{
          firstName: firstName,
          surname: surname,
          otherName: otherName,
          email: email,
          password: Base64.encode(password),
          isLoggedIn: true
        })
      })
      return {isSaved: true};
    }catch (e) {
      return {isSaved: false, message: "Sorry user was not added, please try again"};
    }
  }

  static logout(){
    //There can only be one logged in user, so we are sure this would work
    try {
      let realm = new Realm({schema: [this.#userSchema]});
      let users = realm.objects("Users");
      let loggedInUser = users.filtered('isLoggedIn = true');
     realm.write(()=>{
       loggedInUser.update('isLoggedIn',false);
     })

      return {isLoggedOut: true}
    }catch (e) {
      return {isLoggedOut: false, message: "Sorry, Please try again!"}
      //console.log("error: "+e);
    }
  }

  static login(email, password){
    let realm = new Realm({schema: [this.#userSchema]});
    let users = realm.objects("Users");
    let user = users.filtered('email = $0',email);

    if(user.isEmpty()) return {isValidated: false};

    if(Base64.decode(user[0].password) != password) return {isValidated: false};

    realm.write(()=>{
      user.update('isLoggedIn',true);
    })

    return {isValidated: true, user: user[0]};
  }

  static isUserLoggedIn(){
    let realm = new Realm({schema: [this.#userSchema]});
    let users = realm.objects("Users");
    let loggedInUser = users.filtered('isLoggedIn = true');

    if(loggedInUser.isEmpty()) return {loggedIn: false};
    return {loggedIn: true, user: loggedInUser[0]};
  }
}




