const validations = {
    min_string: /^[A-z]{2,}$/i,
    big_string: /^[A-z]{5,}$/i,
    email: /^[\w]{2,}(\.[A-z]{2,})?@[A-z]{2,}.[A-z]{2,}(\.[A-z]{2,})?$/i,
    password: /^([\w]){4,8}$/i
}

const errorMessages = {
    min_string: {message: "Expected 2 or more characters"},
    big_string: {message: "Expected 5 or more characters"},
    email: {message: "Incorrect email format. Ex. example@gmail.com"},
    password: {message: "Password must be between 4 - 8 characters"}
}

function Validate(key, value) {
    if(validations[key].test(value)) return {validated: true}
    return {validated: false, message: errorMessages[key].message}
}

export default Validate;
