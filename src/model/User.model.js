import {decorate, types} from 'mobx-state-tree';
import {observable} from 'mobx';

const User = types.model('User', {
  firstName: types.string,
  surname: types.string,
  otherName: types.string,
  email: types.string,
  password: types.string,
  isLoggedIn: false,
}).actions(self => ({
      @observable addCurrentUser({firstName, otherName,surname,email,password,isLoggedIn}){
        self.firstName = firstName;
        self.otherName = otherName;
        self.surname = surname;
        self.email = email;
        self.password = password;
        self.isLoggedIn = isLoggedIn;
      }
    })
).create({
  firstName: "",
  surname: "",
  otherName: "",
  email: "",
  password: "",
  isLoggedIn: false,
});

decorate(User, {
  firstName: observable,
  surname: observable,
  otherName: observable,
  email: observable,
  password: observable,
  isLoggedIn: observable,
});

export default User;
